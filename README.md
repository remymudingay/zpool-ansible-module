zpool-ansible-module
====================

An attempt to write an ansible module.
This module creates zpools

Example: 
```
- name: Create a new raidz zpool
  zpool:
    name: zfspool
    devices:
      - /dev/sdc
      - /dev/sdd
      - /dev/sde
    raid_level: raidz
    zil: mirror /dev/sdf /dev/sdg
    vdev: 3
    state: present

- name: Create a new raid 0 stripe zpool
  zpool:
    name: zfspool
    devices:
      - /dev/sdb
      - /dev/sdc
      - /dev/sdd
      - /dev/sde
      - /dev/sdf
    raid_level: none
    l2arc: /dev/sdg
    vdev: 5
    ashift: 12
    state: present

- name: Create a new mirror zpool
  zpool:
    name: rpool
    devices:
      - /dev/sdb
      - /dev/sdc
      - /dev/sdd
      - /dev/sde
    raid_level: mirror
    spare:
      - /dev/sdf
      - /dev/sdg
    zil: mirror /dev/sdh /dev/sdi
    l2arc: /dev/sdj
    autoreplace: on
    ashift: 12
    vdev: 2
    state: present

- name: Create a new mirror zpool with a spare drive
  zpool:
    name: rpool
    devices:
      - /dev/sdc
      - /dev/sdd
    raid_level: mirror
    vdev: 2
    autoreplace: on
    autoexpand: on
    spare:
      - /dev/sde
    state: present

- name: Add devices to an existing zpool
  zpool:
    name: rpool
    add: true
    devices:
    - /dev/sdf
    - /dev/sdg
    raid_level: mirror
    autoexpand: on
    vdev: 2
    state: present

- name: Add spare dev to an existing zpool
  zpool:
    name: rpool
    add: true
    autoreplace: on
    spare:
    - /dev/sdf
    state: present

- name: Set options to an existing zpool
  zpool:
    name: zpool
    set: true
    autoreplace: on
    autoexpand: off
    state: present

- name: Destroy an existing zpool
  zpool:
    name: rpool
    state: absent
```

Requirements
------------

- python 3

Testing
-------

For testing purposes;
1. Firstly create your virtual environemnt.
2. ```git clone https://gitlab.esss.lu.se/remymudingay/zpool-ansible-module.git```
3. ``cd zpool-ansible-module```
4. ```conda install --yes --file requirements.txt``` or virtualenv ...
5. Ensure that the zpool command exists or create a rudimentary executable zpool
   that exists with 0 upon success or 1 or 2 on failure
6. Test: ```./zpool.py ./zpool_del.json```

Status
------
I plan to test locally and if all goes well, I will submit a pull request to the ansible repository.
The module needs to first pass the ```ansible-test sanity``` checks.
License
-------

GNU General Public License v3.0+
https://www.gnu.org/licenses/gpl-3.0.txt
